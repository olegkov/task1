package com.mmtr.task1.service;

import com.mmtr.task1.component.Dictionary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class MyService implements IService {
    private static final Logger logger = LoggerFactory.getLogger(MyService.class);
    private List<Dictionary> dictionaries = new LinkedList<>();
    private Scanner scanner = new Scanner(System.in);
    private Dictionary currentDictionary;

    public MyService(){
        File dictionary1 = new File("dictionary1.dat");
        File dictionary2 = new File("dictionary2.dat");

        Objects.requireNonNull(dictionary1, "Dictionary1 not found");
        Objects.requireNonNull(dictionary2, "Dictionary2 not found");

        String regex1 = "^(?<key>[a-zA-z]{4})$";
        String regex2 = "^^(?<key>\\d{5})$";
        dictionaries.add(new Dictionary(dictionary1, regex1));
        dictionaries.add(new Dictionary(dictionary2, regex2));
    }

    @Override
    public void showDictionaries() {
        for(int i = 0; i < dictionaries.size(); i++){
            logger.info("{}. {}", i, dictionaries.get(i).getName());
        }
    }

    @Override
    public void remove(String key) {
        currentDictionary.remove(key);
    }

    @Override
    public String search(String key) {
        return currentDictionary.search(key);
    }

    @Override
    public void add(String key, String value) {
        currentDictionary.add(key, value);
    }

    @Override
    public Dictionary chooseDictionary() {
        int index;
        while(true){
            logger.info("Choose dictionary: ");
            showDictionaries();
            index = scanner.nextInt();
            if(index > dictionaries.size() - 1 || index < 0){
                logger.info("Incorrect number, try again");
                continue;
            }
            break;
        }

        return dictionaries.get(index);
    }

    @Override
    public void showRecords() {
        currentDictionary.getRecords().forEach((k, v) -> logger.info("{}: {}", k, v));
    }

    public void showMenu(){
        while(true){
            logger.info("1. Choose Dictionary to work");
            logger.info("2. Exit");
            int index = scanner.nextInt();

            switch(index){
                case 1:
                    currentDictionary = chooseDictionary();
                    dictionaryActions();
                    break;
                case 2:
                    logger.info("System closing...");
                    System.exit(0);
                default:
                    break;
            }
        }
    }

    public void dictionaryActions(){
        logger.info("Dictionary Actions:");
        logger.info("1. Show all");
        logger.info("2. Delete");
        logger.info("3. Search");
        logger.info("4. Add");
        logger.info("5. Main menu");

        int innerIndex = scanner.nextInt();

        String key;
        String value;
        switch(innerIndex){
            case 1:
                showRecords();
                break;
            case 2:
                logger.info("Enter key for delete: ");
                key = scanner.next();
                remove(key);
                break;
            case 3:
                logger.info("Enter key for search: ");
                key = scanner.next();
                logger.info("Key: {}, value: {}", key, search(key));
                break;
            case 4:
                logger.info("Enter key: ");
                key = scanner.next();

                logger.info("Enter value: ");
                value = scanner.next();

                add(key, value);
                break;
            case 5:
                showMenu();
                break;
            default:
                break;
        }
    }
}
